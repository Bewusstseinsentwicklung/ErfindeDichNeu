💝 **Ｈｅｒｚｌｉｃｈ &nbsp; Ｗｉｌｌｋｏｍｍｅｎ** 🌷

&nbsp; …am ⛲️ Quellort unserer Gemeinschaft. 👥

<br/>

&nbsp;📦 **Hier findest du folgende Resourcen:**


+ 📄 Dokumentation (🌐 öffentlich):

	+ 🅰️ Allgemeine Informationen 🡺
		+ <small>(Profilseite mit Titel, Kurzinfo, Statistik, Team-Mitgliedern)</small>
	+ 📖 Informationstafel 🡺
		+ <small>**Ausgangspunkt** (jede Reise beginnt hier 🔀)</small>
		+ <small>(Enthält (Basis-)Informationen, Richtlinien, (Link-)Wegweiser,… , Anlaufstellen & FAQ (allgemein))</small>
	+ 🗫 Hilfe - Dokumentation 🡺
		+ <small>(Tipps, Tutorials & Erfahrungen zu verwendeten Plattformen (Telegram, Mumble, Nextcloud - Plugins, …))</small>
		+ <small>(Arbeitsbücher (Skripte), Tipps & Erfahrungen - und vieles mehr.)</small>
	+ 🗫 Team - Dokumentation 🡺
		+ <small>(Basis-Wissen zum Projekt, Vorlagen, )</small>
		+ <small>(Tipps, Tutorials & Erfahrungen zur Administration auf verwendeten Plattformen (Telegram, Mumble, Nextcloud - Plugins, …))</small>
	+ 📜 Richtlinien 🡺
		+ <small>(für Mitglieder, Moderatoren, Helfer & Beitragende)</small>


+ 📄 Dokumentation (🔄 intern):

	+ 🗺 Projektpläne 🡺
		+ 🗂 Strukturelle Planung 🡺
			+ <small>(Ideen, Planung, Umsetzung)</small>
		+ 🔧 Technische Planung 🡺
			+ <small>(Planung, Aufgabenverteilung, Implementation (Umsetzung))</small>
	+ 🗳 Umfragen 🡺
		+ <small>(Quellcode & Sicherungen)</small>
	+ 🔬 Umfrage-Ergebnisse 🡺
		+ <small>(im CSV & PDF - Format)</small>
	+ 📊 Statistiken 🡺
		+ <small>(im CSV & PDF - Format)</small>
	+ 📖 Beitragsrichtlinien (contribution guidelines) 🡺
		+ <small>(für Beitragende)</small>


**Hinweis:** Diese Liste ist im Moment Plan & Referenz der Inhalte. Sobald die entsprechenden Inhalte existieren, wird sie zu einem Inhaltsverzeichnis umgewandelt. (Die Pfeile (🡺) werden dann direkt auf die Dokumente & Verzeichnisse verweisen.)

<br/>

+ **</>** &nbsp; Bei den oben genannten Resourcen handelt es sich fast ausschließlich um Quelldokumente (source code). Eine Ausnahme hiervon bilden bspw. die Hilfe & Team - Dokumentation: Hier befinden sich sowohl Quelldateien, als auch lesbare 📖 Wiki Dokumentation in diesem Verzeichnis.

+ **</>** &nbsp; Im gleichnamigen Verzeichnis in der [**»NA0341-services«**](https://codeberg.org/NA0341-services/Bewusstseinsentwicklung/) - Organisation befindet sich der **Quellcode** für alle mit dem **Projekt** verbundene **Software**.

+ **🔘** &nbsp; Das Fehlerverfolgungssystem (issue tracker) dieses Verzeichnisses (repository) ist zur Dokumentation und Spezifikation geplanter Funktionen, Konzepte und Systeme. Diese werden dann zur Umsetzung <small>(mit Status 🗹)</small> in die Projektpläne übernommen.

<br/>

**Ich lade Dich hiermit ❤️lich dazu ein, diese Inhalte gemeinsam mit uns zu gestalten** 🌷
