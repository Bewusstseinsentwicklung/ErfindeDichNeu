# Titel & Beschreibungen der Gruppen & Kanäle auf Telegram

+ Hier findest du alle Titel, Beschreibungen und informative Nachrichten der Gruppen & Kanäle auf Telegram. Diese sind nach der Struktur unserer Gemeinschaft angeordnet.

+ **Wichtig!:** Ab dem 04. April 2022 sind alle Änderungen an Gruppen - und Kanal - Beschreibungen 1:1 in dieses Dokument zu übertragen. Dabei werden vorherige Inhalte durch die neuen **ersetzt!**
(Durch die Versionierung von Git lassen sich somit alle durchgeführten Veränderungen im Nachhinein einsehen.)

</br>

# Inahltsverzeichnis

[TOC]

# Gruppen

## Eingangsportal

## Hauptgruppe »Bewusstseinsentwicklung«

### Chatgruppe »Bewusstseinsentwicklung«

## Hauptgruppe »Persönlichkeitsentwicklung«

### Chatgruppe »Persönlichkeitsentwicklung«

## Gruppe zur Team-Koordination

# Kanäle

## Kanal für Neuigkeiten

## Kanal zur Team-Koordination

## Kanal für Administrative Aktionen


</br>


# Gruppen


## Eingangsportal


### Gruppen-Name

	Bewusstsein ~ Persönlichkeit ~ Entwicklung ~ Inneres Wachstum


### Gruppen-Beschreibung

	💝 Herzlich Willkommen 🌷
	
	…in der Eingangshalle zu unserer Gemeinschaft
	
	Hier findest du unsere Informationstafel, auf der unsere Regeln & Links zu unseren Gruppen auf Dich warten 😊
	
	Du hast Fragen?
	Wende dich gerne an unser Team (Informationstafel) 🗫


### 1. Gruppen-Nachricht

	💝 ℍ𝕖𝕣𝕫𝕝𝕚𝕔𝕙 𝕎𝕚𝕝𝕝𝕜𝕠𝕞𝕞𝕖𝕟 🌷
	
	…in der Eingangshalle zu unserer Gemeinschaft 🍁
	
	⪧·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⪦
	♾ Wir sind Menschen, die einander unterstützen und gemeinsam wachsen. Dabei nutzen wir Telegram zur Kommunikation. ♾
	⪧·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⩫·⪦
	
	
	**🙪 Du möchtest gerne mehr über uns erfahren?**
	-» Wir haben eine 🪧 Informationstafel 🪧 auf der du alles Wichtige rund um unsere Gemeinschaft findest.🌷
	
	**🙪 Warum gibt es diese Eingangshallen-Gruppe?**
	-» Sie macht unsere Informationstafel besser sichtbar & schützt unsere Gemeinschaft vor unliebsamen Gästen.
	
	***Du bist herzlich dazu eingeladen, unserer stetig wachsenden Gemeinschaft beizutreten.***
	
	**Wir freuen uns sehr auf Dich  (▰˘◡˘▰)**
	
	Herzliche Grüße
	⪩ 𝓑𝓮𝓵𝓪 ⪨
	
	**ErfindeDichNeu**
	⮱ Persönlichkeits- & Bewusstseinsentwicklung


## Hauptgruppe »Bewusstseinsentwicklung«


### Gruppen-Name

	Bewusstseinsentwicklung


### Gruppen-Beschreibung

	💝 Herzlich Willkommen 🌷

	…in der Hauptgruppe für Bewusstseinsentwicklung 🙆

	Hier kannst du dich mit Gleichgesinnten austauschen 💬 und zu deinen Wurzeln finden 🍂

	Infos findest du in der angepinnten 📌 Nachricht.

	Auf Bald 🍁

	Dein Bela. ❤️


### Chatgruppe »Bewusstseinsentwicklung«


### Gruppen-Name

	Chatgruppe für »Bewusstseinsentwicklung«


### Gruppen-Beschreibung

	💝 Herzlich Willkommen 🌷

	…in der Chatgruppe für Bewusstseinsentwicklung 🙆

	Hier kannst du dich mit Gleichgesinnten austauschen 💬 und zu deinen Wurzeln finden 🍂

	Feedback ist willkommen! 😊


## Hauptgruppe »Persönlichkeitsentwicklung«


### Gruppen-Name

	Persönlichkeitsentwicklung


### Gruppen-Beschreibung

	💝 Herzlich Willkommen 🌷
	
	…in der Hauptgruppe zum Thema Persönlichkeitsentwicklung 🙆
	
	Hier kannst du dich mit Gleichgesinnten austauschen 💬 und Dich selbst entdecken 🌈🌷
	
	Infos findest du in der angepinnten 📌 Nachricht.
	
	Auf Bald 🍁
	
	Dein Bela. ❤️


### Chatgruppe »Persönlichkeitsentwicklung«


#### Gruppen-Name

	Chatgruppe für »Persönlichkeitsentwicklung«


#### Gruppen-Beschreibung

	💝 Herzlich Willkommen 🌷
	
	…in der Chatgruppe zum Thema Persönlichkeitsentwicklung 🙆
	
	Hier kannst du dich mit Gleichgesinnten austauschen 💬 und neue Erfahrungen sammeln 🌈🌷
	
	Feedback ist willkommen! 😊


## Gruppe zur Team-Koordination


### Gruppen-Name

	Team-Gruppe für Helfer & Moderatoren


### Gruppen-Beschreibung

	ErfindeDichNeu
	⮱ Persönlichkeits- & Bewusstseinsentwicklung
	
	🗫 Dies ist die Helfer-Zentrale unserer Gemeinschaft
	
	Hier ist:
	-» Aufgabenplanung
	-» Besprechung
	-» Austausch-Runden im Team
	
	Hier führen alle Fäden zusammen 🧶


### 1. Gruppen-Nachricht

*Noch nicht erstellt.*


# Kanäle


## Kanal für Neuigkeiten


### Kanal-Name

	Neuigkeiten, Ankündigungen & Änderungen


### Kanal-Beschreibung

	ErfindeDichNeu
	⮱ Persönlichkeits- & Bewusstseinsentwicklung
	
	Hier bekommst du:
	-» Benachrichtigungen über Änderungen in unserem Quell-Verzeichnis¹
	-» Ankündigungen & Neuigkeiten für unsere Gemeinschaft 🌷
	
	¹ Dort liegen unser Wiki & unsere Dokumentation.


### 1. Kanal-Nachricht

	🍁 **ErfindeDichNeu** 🌷
	⮱ Persönlichkeits- & Bewusstseinsentwicklung
	
	🗫 Hier bekommst du:
	-» Benachrichtigungen über Änderungen in unserem Quell-Verzeichnis¹
	-» Ankündigungen & Neuigkeiten für unsere Gemeinschaft 🌷
	
	**Du möchtest gerne mehr erfahren?**
	…Dann darfst du dir das Verzeichnis¹ gerne einmal ansehen. Wer weiß - vielleicht hast du ein paar Ideen, was wir verbessern können - oder du hilfst uns selbst, diese Gemeinschaft zu einem lichteren, bewussteren Ort zu machen :)
	
	**Du hast Fragen?**
	…Wende dich gerne an unser Team (Informationstafel) 🗫
	
	Herzliche Grüße 👋
	
	Dein Bela. ❤️
	
	－－－－－－－－－－－－－－－－－－－－
	*¹ Das [Quell-Verzeichnis](https://codeberg.org/Bewusstseinsentwicklung/ErfindeDichNeu) enthält die Dokumentation unserer Gemeinschaft. Dazu gehören auch Umfragen und die gesamte Planung.*


## Kanal zur Team-Koordination


### Kanal-Name

	Aufgabenbrettlein für Helfer & Moderatoren


### Kanal-Beschreibung

	ErfindeDichNeu
	⮱ Persönlichkeits- & Bewusstseinsentwicklung
	
	🗫 Dies ist die Helfer-Zentrale unserer Gemeinschaft
	
	Hier ist:
	-» Aufgabenverteilung
	-» Hinweise zu Aufgaben
	-» Austausch-Runden im Team
	
	Hier führen alle Fäden zusammen 🧶


### 1. Kanal-Nachricht

	🍁 **ErfindeDichNeu** 🌷
	⮱ Persönlichkeits- & Bewusstseinsentwicklung
	
	🗫 Dies ist die Helfer-Zentrale unserer Gemeinschaft
	-» Hier werden Aufgaben zusammengetragen, geordnet und zugeteilt.
	-» Jedes Team - Mitglied kann sich für ***die*** Aufgaben eintragen, die es erledigen ***möchte und kann***.
	－－－－－－－－－－－－－－－－－－－－
	
	`<!-- Von hier…`
	
	-» Liste der Aufgabenbereiche (mit Links zu den entsprechenden Nachrichten)
	
	-» Link zur Teamdokumentation
	
	-» Link zu den Team - Richtlinien
	
	`…bis hier ist die Nachricht noch unfertig. -->`
	
	－－－－－－－－－－－－－－－－－－－－
	
	Herzliche Grüße 🍁
	
	Dein Bela. ❤️
	
	－－－－－－－－－－－－－－－－－－－－
	*¹ Natürlich führen nicht nur hier, sondern auch in der Team-Gruppe und an vielen weiteren Orten alle Fäden zusammen - ich finde den Satz einfach toll.* 😉


## Kanal für Administrative Aktionen


### Kanal-Name

	Administrative Aktionen (in unserer Gemeinschaft)


### Kanal-Beschreibung

	ErfindeDichNeu
	⮱ Persönlichkeits- & Bewusstseinsentwicklung
	
	👁‍🗨 Hier werden administrative Aktionen in unserer Gemeinschaft gespeichert. (innerhalb Telegrams)
	
	Dadurch können wir im Zweifelsfall nachsehen, was genau passiert ist.


### 1. Kanal-Nachricht

	This channel has been set as the log channel for Persönlichkeitsentwicklung. All new admin actions will be logged here.

